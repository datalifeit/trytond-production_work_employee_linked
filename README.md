datalife_production_work_employee_linked
========================================

The production_work_employee_linked module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_work_employee_linked/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_work_employee_linked)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
