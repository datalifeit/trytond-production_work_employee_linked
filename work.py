# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Bool
from trytond.pyson import Eval
from trytond.exceptions import UserError
from trytond.i18n import gettext


class WorkCenter(metaclass=PoolMeta):
    __name__ = 'production.work.center'

    linked_center = fields.Many2One('production.work.center', 'Linked center',
        states={'readonly': Bool(Eval('linked'))},
        depends=['linked'])
    center_links = fields.One2Many('production.work.center', 'linked_center',
        'Center links', readonly=True)
    linked = fields.Function(
        fields.Boolean('Linked'), 'get_linked', searcher='search_linked')

    @classmethod
    def __setup__(cls):
        super(WorkCenter, cls).__setup__()
        if cls.employee_allocs.states.get('readonly'):
            cls.employee_allocs.states['readonly'] |= Bool(Eval('linked'))
        else:
            cls.employee_allocs.states['readonly'] = Bool(Eval('linked'))
        if 'linked' not in cls.employee_allocs.depends:
            cls.employee_allocs.depends.append('linked')

    @classmethod
    def validate(cls, records):
        cls._check_linked_centers(records)
        super(WorkCenter, cls).validate(records)

    @classmethod
    def _check_linked_centers(cls, records):
        centers = {}

        def _add_center(center):
            centers.setdefault(center.linked_center.id, []).append(center.id)
            if len(centers[center.linked_center.id]) > 1:
                raise UserError(gettext(
                    'production_work_employee_linked.'
                    'msg_workcenter_unique_link',
                    linked_center=center.linked_center.rec_name))

        for record in records:
            if not record.linked_center:
                continue
            _add_center(record)

        others = cls.search([
            ('id', 'not in', list(map(int, records))),
            ('linked_center', '!=', None)])
        for other in others:
            _add_center(other)

    @classmethod
    def compute_employee(cls, records, date):
        res = super(WorkCenter, cls).compute_employee(records, date)

        centers = cls.search([
            ('linked_center.id', 'in', list(map(int, records)))])

        to_compute = {}
        for center in centers:
            if center.id in res:
                res[center.linked_center.id] = res[center.id]
            else:
                to_compute.setdefault(center.id, [])
                to_compute[center.id].append(center.linked_center.id)

        if to_compute:
            others = cls.compute_employee(cls.browse(
                list(to_compute.keys())), date)
            for center_id, employee_id in others.items():
                for item in to_compute[center_id]:
                    res[item] = employee_id
        return res

    @classmethod
    def get_linked(cls, records, name=None):
        res = dict.fromkeys(list(map(int, records)), False)
        centers = cls.search_read([
            ('linked_center.id', 'in', list(map(int, records)))],
            fields_names=['linked_center'])

        res.update({
            center['linked_center']: True for center in centers})
        return res

    @classmethod
    def search_linked(cls, name, clause):
        reverse = {
            '=': '!=',
            '!=': '='}
        if clause[1] in reverse:
            if (clause[2] and clause[1] == '=') or \
                    (not clause[2] and clause[1] == '!='):
                return [('center_links', reverse[clause[1]], None)]
            else:
                return [('center_links', clause[1], None)]
        return []


class EmployeeWorkCenter(metaclass=PoolMeta):
    __name__ = 'company.employee-work.center'

    @classmethod
    def __setup__(cls):
        super(EmployeeWorkCenter, cls).__setup__()
        if not cls.work_center.domain:
            cls.work_center.domain = []
        cls.work_center.domain.append(('linked', '=', False))


class WorkCenterEmployeeRotate(metaclass=PoolMeta):
    __name__ = 'production.work.center.employee_rotate'

    def _get_work_centers_to_rotate(self):
        centers = super()._get_work_centers_to_rotate()
        return [c for c in centers if not c.center_links]
