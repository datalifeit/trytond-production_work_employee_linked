#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "model:ir.message,text:msg_workcenter_unique_link"
msgid "Center \"%(linked_center)s\" cannot be linked to many centers."
msgstr "El centro \"%(linked_center)s\" no puede estar vinculado a varios centros."

msgctxt "field:production.work.center,center_links:"
msgid "Center links"
msgstr "Vínculos a centros"

msgctxt "field:production.work.center,linked:"
msgid "Linked"
msgstr "Vinculado"

msgctxt "field:production.work.center,linked_center:"
msgid "Linked center"
msgstr "Centro vinculado"
