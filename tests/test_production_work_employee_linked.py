# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import datetime

from trytond.exceptions import UserError
from trytond.pool import Pool
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.modules.company.tests import create_company, set_company


class ProductionWorkEmployeeLinkedTestCase(ModuleTestCase):
    """Test Production Work Employee Linked module"""
    module = 'production_work_employee_linked'

    def configure_work_centers(self):
        pool = Pool()
        Location = pool.get('stock.location')
        WorkCenter = pool.get('production.work.center')

        warehouse, = Location.search([('type', '=', 'warehouse')])
        wcenter1 = WorkCenter(name='Work Center 1', warehouse=warehouse)
        wcenter1.save()
        wcenter2 = WorkCenter(name='Work Center 2', warehouse=warehouse)
        wcenter2.save()
        wcenter3 = WorkCenter(name='Work Center 3', warehouse=warehouse)
        wcenter3.save()
        wcenter4 = WorkCenter(name='Work Center 4', warehouse=warehouse)
        wcenter4.save()
        wcenter1.linked_center = wcenter4
        wcenter1.save()

        work_centers = [
            (datetime.date(2014, 1, 1), None, wcenter1),
            (datetime.date(2015, 1, 1), datetime.date(2015, 12, 31), wcenter2),
            (datetime.date(2016, 1, 1), datetime.date(2016, 12, 31), wcenter3),
            (datetime.date(2016, 1, 2), datetime.date(2016, 1, 2), wcenter2),
            (None, None, wcenter4)
        ]
        test_work_centers = [
            (datetime.date(2013, 1, 1), None),
            (datetime.date(2014, 1, 1), wcenter1),
            (datetime.date(2014, 6, 1), wcenter1),
            (datetime.date(2015, 1, 1), wcenter2),
            (datetime.date(2015, 6, 1), wcenter2),
            (datetime.date(2016, 1, 1), wcenter3),
            (datetime.date(2016, 1, 2), wcenter2),
            (datetime.date(2016, 1, 3), wcenter3),
            (datetime.date(2016, 6, 1), wcenter3),
            (datetime.date(2014, 1, 1), wcenter4),
            (datetime.date(2014, 6, 1), wcenter4),
            ]
        return work_centers, test_work_centers


    @with_transaction()
    def test_work_center_link_restriction(self):
        """Test compute_work_center"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        Location = pool.get('stock.location')
        EmployeeWorkCenter = pool.get('company.employee-work.center')
        WorkCenter = pool.get('production.work.center')

        company = create_company()
        with set_company(company):
            warehouse, = Location.search([('type', '=', 'warehouse')])
            wcenter4 = WorkCenter(name='Work Center 4', warehouse=warehouse)
            wcenter4.save()
            wcenter1 = WorkCenter(name='Work Center 1', warehouse=warehouse,
                linked_center=wcenter4)
            wcenter1.save()
            wcenter2 = WorkCenter(name='Work Center 2', warehouse=warehouse,
                linked_center=wcenter4)
            self.assertRaises(UserError, wcenter2.save)

    @with_transaction()
    def test_employee_work_center_restriction(self):
        """Test compute_work_center"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        Location = pool.get('stock.location')
        EmployeeWorkCenter = pool.get('company.employee-work.center')
        WorkCenter = pool.get('production.work.center')

        company = create_company()
        with set_company(company):
            warehouse, = Location.search([('type', '=', 'warehouse')])
            wcenter4 = WorkCenter(name='Work Center 4', warehouse=warehouse)
            wcenter4.save()
            wcenter1 = WorkCenter(name='Work Center 1', warehouse=warehouse,
                linked_center=wcenter4)
            wcenter1.save()
            party = Party(name='Pam Beesly')
            party.save()
            employee = Employee(party=party.id, company=company)
            employee.save()
            res = EmployeeWorkCenter(
                    employee=employee,
                    date=datetime.date(2014, 1, 1),
                    work_center=wcenter4)
            self.assertRaises(UserError, res.save)

    @with_transaction()
    def test_compute_employee_work_center(self):
        """Test compute_work_center"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        EmployeeWorkCenter = pool.get('company.employee-work.center')
        WorkCenter = pool.get('production.work.center')

        company = create_company()
        with set_company(company):
            work_centers, test_work_centers = self.configure_work_centers()
            party = Party(name='Pam Beesly')
            party.save()
            employee = Employee(party=party.id, company=company)
            employee.save()
            for date, end_date, work_center in work_centers:
                if not date:
                    wc4 = work_center
                    continue
                res = EmployeeWorkCenter(
                    employee=employee,
                    date=date,
                    end_date=end_date,
                    work_center=work_center)
                res.save()
            for date, work_center in test_work_centers:
                if work_center != wc4:
                    self.assertEqual(employee.compute_work_center(date),
                        work_center and work_center.id or None)
                if work_center:
                    self.assertEqual(WorkCenter.compute_employee(
                        [work_center], date), {work_center.id: employee.id})


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductionWorkEmployeeLinkedTestCase))
    return suite
